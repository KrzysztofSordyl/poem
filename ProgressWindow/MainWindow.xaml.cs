﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProgressWindow
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public MainWindow(Progress p) : this()
        {
            this.DataContext = p;
        }
    }

    public class Progress : INotifyPropertyChanged
    {
        private int min = 0;
        private int max = 100;
        private int value = 0;

        public int Min
        {
            get
            {
                return min;
            }

            set
            {
                min = value;
                this.propertyChanged("Min");
            }
        }

        public int Max
        {
            get
            {
                return max;
            }

            set
            {
                max = value;
                this.propertyChanged("Max");
                this.propertyChanged("ProgressText");
            }
        }

        public int Value
        {
            get
            {
                return value;
            }

            set
            {
                this.value = value;
                this.propertyChanged("Value");
                this.propertyChanged("ProgressText");
            }
        }
        
        public string ProgressText
        {
            get
            {
                return this.Value + "/" + this.Max;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void propertyChanged(string name)
        {
            if (this.PropertyChanged != null) this.PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}
