﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities.Interfaces
{
    /// <summary>
    /// Interface which indicates class is cloneable (and it has type! WOW!)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ICloneable<T> : ICloneable
    {
        /// <summary>
        /// Standard clone method but...
        /// </summary>
        /// <returns>... miracle! It has its own type!</returns>
        T CloneThis();
    }
}
