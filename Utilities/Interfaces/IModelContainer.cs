﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities.Interfaces
{
    /// <summary>
    /// Interface which indicates class contains any model
    /// </summary>
    /// <typeparam name="T">type of model</typeparam>
    public interface IModelContainer<T>
    {
        /// <summary>
        /// Method which give model
        /// </summary>
        /// <returns>model</returns>
        T GetModel();
    }
}
