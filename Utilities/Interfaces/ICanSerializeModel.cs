﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities.Interfaces
{
    /// <summary>
    /// Interface which indicates class can serialize and deserialize owned model
    /// </summary>
    public interface ICanSerializeModel
    {
        /// <summary>
        /// Serialize to file
        /// </summary>
        /// <param name="filename">path to file</param>
        void SaveXml(string filename);

        /// <summary>
        /// Deserialize from file
        /// </summary>
        /// <param name="filename">path to file</param>
        void LoadXml(string filename);
    }
}
