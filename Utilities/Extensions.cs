﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    /// <summary>
    /// Class which contains extension methods (which usually should be written in framework)
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Yeah! Now you can Sort some IEnumerable beetwen linq methods :)
        /// </summary>
        /// <typeparam name="T">type of elements</typeparam>
        /// <returns>sorted IEnumerable</returns>
        public static IEnumerable<T> Sort<T>(this IEnumerable<T> self) where T : IComparable
        {
            List<T> list = self.ToList();
            list.Sort();
            return list;
        }
    }
}
