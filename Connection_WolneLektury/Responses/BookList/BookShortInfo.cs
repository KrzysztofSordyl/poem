﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Utilities.Interfaces;

namespace Connection.Responses.BookList
{
    /// <summary>
    /// Model class for book short info
    /// </summary>
    public class BookShortInfo : IEquatable<BookShortInfo> , ICloneable<BookShortInfo>
    {
        /// <summary>
        /// Kind of book
        /// </summary>
        [XmlElement("kind")]
        public string Kind { get; set; }

        /// <summary>
        /// Author of book
        /// </summary>
        [XmlElement("author")]
        public string Author { get; set; }

        /// <summary>
        /// Url to website of book
        /// </summary>
        [XmlElement("url")]
        public string Url { get; set; }

        /// <summary>
        /// Title of book
        /// </summary>
        [XmlElement("title")]
        public string Title { get; set; }

        /// <summary>
        /// Url to cover of book
        /// </summary>
        [XmlElement("cover")]
        public string Cover { get; set; }

        /// <summary>
        /// Epoch of book
        /// </summary>
        [XmlElement("epoch")]
        public string Epoch { get; set; }

        /// <summary>
        /// Url to API of book
        /// </summary>
        [XmlElement("href")]
        public string Href { get; set; }

        /// <summary>
        /// Genre of book
        /// </summary>
        [XmlElement("genre")]
        public string Genre { get; set; }

        /// <summary>
        /// Url to cover thumb of book
        /// </summary>
        [XmlElement("cover_thumb")]
        public string CoverThumb { get; set; }

        /// <summary>
        /// Standard equal method
        /// </summary>
        public bool Equals(BookShortInfo other)
        {
            if ((object)other == null) return false;
            if (this.Kind != other.Kind) return false;
            if (this.Author != other.Author) return false;
            if (this.Url != other.Url) return false;
            if (this.Title != other.Title) return false;
            if (this.Cover != other.Cover) return false;
            if (this.Epoch != other.Epoch) return false;
            if (this.Href != other.Href) return false;
            if (this.Genre != other.Genre) return false;
            if (this.CoverThumb != other.CoverThumb) return false;
            return true;
        }

        /// <summary>
        /// Standard equal method
        /// </summary>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as BookShortInfo);
        }

        /// <summary>
        /// Method which clone this
        /// </summary>
        /// <returns>clone with suitable type</returns>
        public BookShortInfo CloneThis()
        {
            return (this as ICloneable).Clone() as BookShortInfo;
        }

        /// <summary>
        /// Method which clone this
        /// </summary>
        /// <returns>clone with not suitable type</returns>
        public object Clone()
        {
            return this.MemberwiseClone();
        }

        /// <summary>
        /// Operator == executes Equal method
        /// </summary>
        /// <param name="bsi1">left operand</param>
        /// <param name="bsi2">right operand</param>
        public static bool operator ==(BookShortInfo bsi1, BookShortInfo bsi2)
        {
            if ((object)bsi1 == null ^ (object)bsi2 == null) return false;
            if ((object)bsi1 == null && (object)bsi2 == null) return true;
            return bsi1.Equals(bsi2);
        }

        /// <summary>
        /// Operator != executes Equal method
        /// </summary>
        /// <param name="bsi1">left operand</param>
        /// <param name="bsi2">right operand</param>
        public static bool operator !=(BookShortInfo bsi1, BookShortInfo bsi2)
        {
            if ((object)bsi1 == null ^ (object)bsi2 == null) return true;
            if ((object)bsi1 == null && (object)bsi2 == null) return false;
            return !bsi1.Equals(bsi2);
        }

        /// <summary>
        /// Method executes base.GetHashCode
        /// </summary>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #region example
        //<kind>Liryka</kind>
        //<author>Guillaume Apollinaire</author>
        //<url>
        //http://wolnelektury.pl/katalog/lektura/nim-prog-przestapilem-celi/
        //</url>
        //<title>(Nim próg przestąpiłem celi...)</title>
        //<cover>book/cover/nim-prog-przestapilem-celi.jpg</cover>
        //<epoch>Modernizm</epoch>
        //<href>
        //http://wolnelektury.pl/api/books/nim-prog-przestapilem-celi/
        //</href>
        //<genre>Wiersz</genre>
        //<cover_thumb>book/cover_thumb/nim-prog-przestapilem-celi.jpg</cover_thumb>
        #endregion
    }
}
