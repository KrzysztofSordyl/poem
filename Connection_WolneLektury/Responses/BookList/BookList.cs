﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Utilities.Interfaces;

namespace Connection.Responses.BookList
{
    /// <summary>
    /// Model class for list of books
    /// </summary>
    [XmlRoot("response")]
    public class BooksList : IEquatable<BooksList>, ICloneable<BooksList>
    {
        /// <summary>
        /// List of books' short info
        /// </summary>
        [XmlElement("resource")]
        public List<BookShortInfo> Resources { get; set; }

        /// <summary>
        /// Standard equal method
        /// </summary>
        public bool Equals(BooksList other)
        {
            if ((object)other == null) return false;
            if (!this.Resources.SequenceEqual(other.Resources)) return false;
            return true;
        }

        /// <summary>
        /// Standard equal method
        /// </summary>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as BooksList);
        }

        /// <summary>
        /// Method which clone this
        /// </summary>
        /// <returns>clone with suitable type</returns>
        public BooksList CloneThis()
        {
            return (this as ICloneable).Clone() as BooksList;
        }

        /// <summary>
        /// Method which clone this
        /// </summary>
        /// <returns>clone with not suitable type</returns>
        public object Clone()
        {
            BooksList books = this.MemberwiseClone() as BooksList;
            books.Resources = this.Resources.Select(x => x.CloneThis()).ToList();
            return books;
        }

        /// <summary>
        /// Operator == executes Equal method
        /// </summary>
        /// <param name="bl1">left operand</param>
        /// <param name="bl2">right operand</param>
        public static bool operator ==(BooksList bl1, BooksList bl2)
        {
            if ((object)bl1 == null ^ (object)bl2 == null) return false;
            if ((object)bl1 == null && (object)bl2 == null) return true;
            return bl1.Equals(bl2);
        }

        /// <summary>
        /// Operator != executes Equal method
        /// </summary>
        /// <param name="bl1">left operand</param>
        /// <param name="bl2">right operand</param>
        public static bool operator !=(BooksList bl1, BooksList bl2)
        {
            if ((object)bl1 == null ^ (object)bl2 == null) return true;
            if ((object)bl1 == null && (object)bl2 == null) return false;
            return !bl1.Equals(bl2);
        }

        /// <summary>
        /// Method executes base.GetHashCode
        /// </summary>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
