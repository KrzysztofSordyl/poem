﻿using Connection.Responses.BookList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Utilities.Interfaces;

namespace Connection.Responses.BookInfo
{
    /// <summary>
    /// Model class for full book info
    /// </summary>
    [XmlRoot("response")]
    public class BookInfo : IEquatable<BookInfo>, ICloneable<BookInfo>
    {
        /// <summary>
        /// Url to xml version of book
        /// </summary>
        [XmlElement("xml")]
        public string Xml { get; set; }

        /// <summary>
        /// Url to fictionbook version of book
        /// </summary>
        [XmlElement("fb2")]
        public string Fb2 { get; set; }
        
        /// <summary>
        /// List of genres
        /// </summary>
        [XmlArray("genres")]
        [XmlArrayItem("resource")]
        public List<Resource> Genres { get; set; }

        /// <summary>
        /// Url to mobi version of book
        /// </summary>
        [XmlElement("mobi")]
        public string Mobi { get; set; }

        /// <summary>
        /// List of kinds
        /// </summary>
        [XmlArray("kinds")]
        [XmlArrayItem("resource")]
        public List<Resource> Kinds { get; set; }

        /// <summary>
        /// Short info about parent
        /// </summary>
        [XmlElement("parent")]
        public BookShortInfo Parent { get; set; }

        /// <summary>
        /// Title of book
        /// </summary>
        [XmlElement("title")]
        public string Title { get; set; }

        /// <summary>
        /// Url to website of book
        /// </summary>
        [XmlElement("url")]
        public string Url { get; set; }

        /// <summary>
        /// List of media
        /// </summary>
        [XmlArray("media")]
        [XmlArrayItem("resource")]
        public List<MediaResource> Media { get; set; }

        /// <summary>
        /// Url to over of book
        /// </summary>
        [XmlElement("cover")]
        public string Cover { get; set; }

        /// <summary>
        /// List of epochs
        /// </summary>
        [XmlArray("epochs")]
        [XmlArrayItem("resource")]
        public List<Resource> Epochs { get; set; }

        /// <summary>
        /// Url to html version of book
        /// </summary>
        [XmlElement("html")]
        public string Html { get; set; }

        /// <summary>
        /// List of authors
        /// </summary>
        [XmlArray("authors")]
        [XmlArrayItem("resource")]
        public List<Resource> Authors { get; set; }

        /// <summary>
        /// Url to pdf version of book
        /// </summary>
        [XmlElement("pdf")]
        public string Pdf { get; set; }

        /// <summary>
        /// Url to txt version of book
        /// </summary>
        [XmlElement("txt")]
        public string Txt { get; set; }

        /// <summary>
        /// List of child books' short info
        /// </summary>
        [XmlArray("children")]
        [XmlArrayItem("resource")]
        public List<BookShortInfo> Children { get; set; }

        /// <summary>
        /// Url to epub version of book
        /// </summary>
        [XmlElement("epub")]
        public string Epub { get; set; }

        /// <summary>
        /// Url to cover thumb version of book
        /// </summary>
        [XmlElement("cover_thumb")]
        public string CoverThumb { get; set; }

        /// <summary>
        /// Standard equal method
        /// </summary>
        public bool Equals(BookInfo other)
        {
            if ((object)other == null) return false;
            if (this.Xml != other.Xml) return false;
            if (this.Fb2 != other.Fb2) return false;
            if (!this.Genres.SequenceEqual(other.Genres)) return false;
            if (this.Mobi != other.Mobi) return false;
            if (!this.Kinds.SequenceEqual(other.Kinds)) return false;
            if (this.Parent != other.Parent) return false;
            if (this.Title != other.Title) return false;
            if (this.Url != other.Url) return false;
            if (!this.Media.SequenceEqual(other.Media)) return false;
            if (this.Cover != other.Cover) return false;
            if (!this.Epochs.SequenceEqual(other.Epochs)) return false;
            if (this.Html != other.Html) return false;
            if (!this.Authors.SequenceEqual(other.Authors)) return false;
            if (this.Pdf != other.Pdf) return false;
            if (this.Txt != other.Txt) return false;
            if (!this.Children.SequenceEqual(other.Children)) return false;
            if (this.Epub != other.Epub) return false;
            if (this.CoverThumb != other.CoverThumb) return false;
            return true;
        }

        /// <summary>
        /// Standard equal method
        /// </summary>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as BookInfo);
        }

        /// <summary>
        /// Method which clone this
        /// </summary>
        /// <returns>clone with suitable type</returns>
        public BookInfo CloneThis()
        {
            return (this as ICloneable).Clone() as BookInfo;
        }

        /// <summary>
        /// Method which clone this
        /// </summary>
        /// <returns>clone with not suitable type</returns>
        public object Clone()
        {
            BookInfo info = this.MemberwiseClone() as BookInfo;
            info.Genres = this.Genres.Select(x => x.CloneThis()).ToList();
            info.Kinds = this.Kinds.Select(x => x.CloneThis()).ToList();
            info.Parent = this.Parent.CloneThis();
            info.Media = this.Media.Select(x => x.CloneThis()).ToList();
            info.Epochs = this.Epochs.Select(x => x.CloneThis()).ToList();
            info.Authors = this.Authors.Select(x => x.CloneThis()).ToList();
            info.Children = this.Children.Select(x => x.CloneThis()).ToList();
            return info;
        }

        /// <summary>
        /// Operator == executes Equal method
        /// </summary>
        /// <param name="bi1">left operand</param>
        /// <param name="bi2">right operand</param>
        public static bool operator ==(BookInfo bi1, BookInfo bi2)
        {
            if ((object)bi1 == null ^ (object)bi2 == null) return false;
            if ((object)bi1 == null && (object)bi2 == null) return true;
            return bi1.Equals(bi2);
        }

        /// <summary>
        /// Operator != executes Equal method
        /// </summary>
        /// <param name="bi1">left operand</param>
        /// <param name="bi2">right operand</param>
        public static bool operator !=(BookInfo bi1, BookInfo bi2)
        {
            if ((object)bi1 == null ^ (object)bi2 == null) return false;
            if ((object)bi1 == null && (object)bi2 == null) return true;
            return !bi1.Equals(bi2);
        }

        /// <summary>
        /// Method executes base.GetHashCode
        /// </summary>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
