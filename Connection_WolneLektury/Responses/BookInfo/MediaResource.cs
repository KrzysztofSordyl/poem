﻿using System;
using System.Xml.Serialization;
using Utilities.Interfaces;

namespace Connection.Responses.BookInfo
{
    /// <summary>
    /// Model class for media resource which contains website url, director, type, name and artist
    /// </summary>
    [XmlRoot("resource")]
    public class MediaResource : IEquatable<MediaResource>, ICloneable<MediaResource>
    {
        /// <summary>
        /// Url to website of media
        /// </summary>
        [XmlElement("url")]
        public string Url { get; set; }

        /// <summary>
        /// Url to director of media
        /// </summary>
        [XmlElement("director")]
        public string Director { get; set; }

        /// <summary>
        /// Url to type of media
        /// </summary>
        [XmlElement("type")]
        public string Type { get; set; }

        /// <summary>
        /// Url to name of media
        /// </summary>
        [XmlElement("name")]
        public string Name { get; set; }

        /// <summary>
        /// Url to artist of media
        /// </summary>
        [XmlElement("artist")]
        public string Artist { get; set; }

        /// <summary>
        /// Standard equal method
        /// </summary>
        public bool Equals(MediaResource other)
        {
            if ((object)other == null) return false;
            if (this.Url != other.Url) return false;
            if (this.Director != other.Director) return false;
            if (this.Type != other.Type) return false;
            if (this.Name != other.Name) return false;
            if (this.Artist != other.Artist) return false;
            return true;
        }

        /// <summary>
        /// Standard equal method
        /// </summary>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as MediaResource);
        }

        /// <summary>
        /// Method which clone this
        /// </summary>
        /// <returns>clone with suitable type</returns>
        public MediaResource CloneThis()
        {
            return (this as ICloneable).Clone() as MediaResource;
        }

        /// <summary>
        /// Method which clone this
        /// </summary>
        /// <returns>clone with not suitable type</returns>
        public object Clone()
        {
            return this.MemberwiseClone();
        }

        /// <summary>
        /// Operator == executes Equal method
        /// </summary>
        /// <param name="mr1">left operand</param>
        /// <param name="mr2">right operand</param>
        public static bool operator ==(MediaResource mr1, MediaResource mr2)
        {
            if ((object)mr1 == null ^ (object)mr2 == null) return false;
            if ((object)mr1 == null && (object)mr2 == null) return true;
            return mr1.Equals(mr2);
        }

        /// <summary>
        /// Operator != executes Equal method
        /// </summary>
        /// <param name="mr1">left operand</param>
        /// <param name="mr2">right operand</param>
        public static bool operator !=(MediaResource mr1, MediaResource mr2)
        {
            if ((object)mr1 == null ^ (object)mr2 == null) return true;
            if ((object)mr1 == null && (object)mr2 == null) return false;
            return !mr1.Equals(mr2);
        }

        /// <summary>
        /// Method executes base.GetHashCode
        /// </summary>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}