﻿using System;
using System.Xml.Serialization;
using Utilities.Interfaces;

namespace Connection.Responses.BookInfo
{
    /// <summary>
    /// Model class for resource which contains website url, API url and name
    /// </summary>
    public class Resource : IEquatable<Resource>, ICloneable<Resource>
    {
        /// <summary>
        /// Url to website of resource
        /// </summary>
        [XmlElement("url")]
        public string Url { get; set; }

        /// <summary>
        /// Url to API of resource
        /// </summary>
        [XmlElement("href")]
        public string Href { get; set; }

        /// <summary>
        /// Name of resource
        /// </summary>
        [XmlElement("name")]
        public string Name { get; set; }

        /// <summary>
        /// Standard equal method
        /// </summary>
        public bool Equals(Resource other)
        {
            if ((object)other == null) return false;
            if (this.Url != other.Url) return false;
            if (this.Href != other.Href) return false;
            if (this.Name != other.Name) return false;
            return true;
        }

        /// <summary>
        /// Standard equal method
        /// </summary>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as Resource);
        }

        /// <summary>
        /// Method which clone this
        /// </summary>
        /// <returns>clone with suitable type</returns>
        public Resource CloneThis()
        {
            return (this as ICloneable).Clone() as Resource;
        }

        /// <summary>
        /// Method which clone this
        /// </summary>
        /// <returns>clone with not suitable type</returns>
        public object Clone()
        {
            return this.MemberwiseClone();
        }

        /// <summary>
        /// Operator == executes Equal method
        /// </summary>
        /// <param name="r1">left operand</param>
        /// <param name="r2">right operand</param>
        public static bool operator ==(Resource r1, Resource r2)
        {
            if ((object)r1 == null ^ (object)r2 == null) return false;
            if ((object)r1 == null && (object)r2 == null) return true;
            return r1.Equals(r2);
        }

        /// <summary>
        /// Operator != executes Equal method
        /// </summary>
        /// <param name="r1">left operand</param>
        /// <param name="r2">right operand</param>
        public static bool operator !=(Resource r1, Resource r2)
        {
            if ((object)r1 == null ^ (object)r2 == null) return true;
            if ((object)r1 == null && (object)r2 == null) return false;
            return !r1.Equals(r2);
        }

        /// <summary>
        /// Method executes base.GetHashCode
        /// </summary>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}