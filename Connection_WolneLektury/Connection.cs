﻿using Connection.Responses.BookInfo;
using Connection.Responses.BookList;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Connection
{
    /// <summary>
    /// Class which is responsible for connection with Wolne Lektury API
    /// </summary>
    public static class ConnectionWL
    {
        private static RestClient client = new RestClient("http://wolnelektury.pl/") { Timeout = 20000 };

        private const string URLPoems = "api/genres/wiersz/books";
        private const string Format = "?format=xml";
        private const string UrlMedia = "http://wolnelektury.pl/media/";
        
        /// <summary>
        /// Method which gets list of poems
        /// </summary>
        /// <returns>contains list of BookShortInfo</returns>
        public static BooksList GetPoemsList()
        {
            RestRequest request = new RestRequest(URLPoems + Format);
            RestResponse response = (RestResponse)client.Execute(request);

            XmlSerializer serializer = new XmlSerializer(typeof(BooksList));
            TextReader reader = new StringReader(response.Content);
            BooksList list = serializer.Deserialize(reader) as BooksList;
            list.Resources.ForEach(x => { x.Cover = UrlMedia + x.Cover; x.CoverThumb = UrlMedia + x.CoverThumb; });
            return list;
        }

        /// <summary>
        /// Method which gets info about book
        /// </summary>
        /// <param name="book">short info about book which info about is returned</param>
        /// <returns>full info about book</returns>
        public static BookInfo GetBookInfo(BookShortInfo book)
        {
            RestRequest request = new RestRequest(getUrlFragment(book.Href) + Format);
            RestResponse response = (RestResponse)client.Execute(request);

            XmlSerializer serializer = new XmlSerializer(typeof(BookInfo));
            TextReader reader = new StringReader(response.Content);
            BookInfo info = serializer.Deserialize(reader) as BookInfo;
            return info;
        }

        /// <summary>
        /// Method which gets text of book
        /// </summary>
        /// <param name="book">fuul info about book which is returned</param>
        /// <returns>string which contains text of book</returns>
        public static string GetBook(BookInfo book)
        {
            RestRequest request = new RestRequest(getUrlFragment(book.Txt));
            return client.Execute(request).Content;
        }

        private static string getUrlFragment(string url)
        {
            string urlFragment = url;
            List<string> words = new List<string>()
            {
                "http://",
                "wolnelektury.pl/"
            };
            foreach (string word in words)
            {
                if (urlFragment.StartsWith(word)) urlFragment = urlFragment.Substring(word.Length);
                else break;
            }
            return urlFragment;
        }
    }
}
