﻿using Poem.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

namespace Poem.VisualElements
{
    /// <summary>
    /// UserControl which visualize BookShortInfoViewModel
    /// </summary>
    public partial class BookShortInfoControl : UserControl
    {
        /// <summary>
        /// Simply control constructor
        /// </summary>
        public BookShortInfoControl()
        {
            InitializeComponent();
        }

        private BookInfoViewModel fullInfo;

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (this.fullInfo == null)
            {
                if (this.DataContext is BookShortInfoViewModel && (this.DataContext as BookShortInfoViewModel).Href != null)
                {
                    this.fullInfo = (this.DataContext as BookShortInfoViewModel).GetMoreInfo();
                    this.showFullInfo();
                }
            }
            else this.showFullInfo();
        }

        private void showFullInfo()
        {
            if (this.fullInfo != null)
            {
                Window w = new Window()
                {
                    Content = new BookInfoControl() { DataContext = this.fullInfo },
                    SizeToContent = SizeToContent.WidthAndHeight,
                    Title = this.fullInfo.Title
                };
                w.ShowDialog();
            }
        }
    }

    /// <summary>
    /// Converter which verify BookShortInfoViewModel is empty
    /// </summary>
    public class BookShortInfoToVisibilityConverter : IValueConverter
    {
        /// <summary>
        /// Methods which converts value
        /// </summary>
        /// <param name="value">value to convert (BookShortInfoViewModel)</param>
        /// <param name="targetType">not neede</param>
        /// <param name="parameter">not needed</param>
        /// <param name="culture">not needed</param>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value == null || (value as BookShortInfoViewModel).Title == null ? Visibility.Collapsed : Visibility.Visible;
        }

        /// <summary>
        /// Method not needed. Don't execute.
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException(); // Nie potrzebne
        }
    }
}
/*using Poem.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Poem.VisualElements
{
    public partial class BookShortInfoControl : UserControl
    {
        public BookShortInfoControl()
        {
            InitializeComponent();
        }

        private BookInfoViewModel fullInfo;

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (this.fullInfo == null)
            {
                if (this.DataContext is BookShortInfoViewModel)
                {
                    this.fullInfo = (this.DataContext as BookShortInfoViewModel).GetMoreInfo();
                    this.showFullInfo();
                }
            }
            else this.showFullInfo();
        }

        private void showFullInfo()
        {
            //this.bicFullInfo.DataContext = this.fullInfo;
            //this.bicFullInfo.Visibility = Visibility.Visible;
            Window parentWindow = Window.GetWindow(this);
            FrameworkElement element = parentWindow.Content as FrameworkElement;
            AdornerLayer layer = AdornerLayer.GetAdornerLayer(element);
            layer.Add(new BookInfoAdorner(this) { DataContext = this.fullInfo });
        }
    }

    public class BookInfoAdorner : Adorner
    {
        public BookInfoControl FullInfo = new BookInfoControl();
        public new object DataContext
        {
            set
            {
                this.FullInfo.DataContext = value;
            }
        }

        public BookInfoAdorner(UIElement adornedElement) : base(adornedElement)
        {

        }
        protected override Visual GetVisualChild(int index)
        {
            return this.FullInfo;
        }
        protected override int VisualChildrenCount { get { return 1; } }

        protected override Size ArrangeOverride(Size finalSize)
        {
            double controlWidth = AdornedElement.DesiredSize.Width;
            double controlHeight = AdornedElement.DesiredSize.Height;

            this.FullInfo.Arrange(new Rect(controlWidth, 0, 100, 100));

            return finalSize;
        }
    }
}
*/
