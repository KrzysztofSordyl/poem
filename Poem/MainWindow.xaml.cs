﻿using Connection;
using Connection.Responses.BookInfo;
using Connection.Responses.BookList;
using Poem.ViewModels;
using Poem.VisualElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Poem
{
    /// <summary>
    /// Main window of Poem
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Simple constructor for Window
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            BookListViewModel model;
            do
            {
                try
                {
                    model = new BookListViewModel(ConnectionWL.GetPoemsList(), true);
                }
                catch
                {
                    model = null;
                    if (MessageBox.Show("Błąd pobierania. Czy chcesz spróbować ponownie?", "Error", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                    {
                        this.Close();
                        return;
                    }
                }
            } while (model == null);
            
            model.Filter += filter;
            this.DataContext = model;
            this.wbPoem.Navigated += this.Navigated;
        }

        private void filter(Predicate<object> predicate)
        {
            this.lbPoems.Items.Filter = predicate;
        }

        private void Navigated(object sender, NavigationEventArgs e)
        {
            dynamic domDoc = this.wbPoem.Document;
            domDoc.charset = "Unicode";
        }

        private void lbPoems_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                this.wbPoem.Source = new Uri(ConnectionWL.GetBookInfo((e.AddedItems[0] as BookShortInfoViewModel).GetModel()).Html);
            }
            catch
            {
                // Coś poszło nie tak ale się tym nie przejmujemy :)
            }
        }
    }
}
