﻿using Connection;
using Connection.Responses.BookInfo;
using Connection.Responses.BookList;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Utilities.Interfaces;

namespace Poem.ViewModels
{
    /// <summary>
    /// ViewModel class for full book info
    /// </summary>
    public class BookInfoViewModel : INotifyPropertyChanged, ICanSerializeModel, IModelContainer<BookInfo>
    {
        private BookInfo info;

        /// <summary>
        /// Simply ViewModel constructor
        /// </summary>
        /// <param name="info">model</param>
        public BookInfoViewModel(BookInfo info)
        {
            this.info = info;
        }

        /// <summary>
        /// Title of book
        /// </summary>
        public string Title
        {
            get
            {
                return this.info.Title;
            }
            set
            {
                this.info.Title = value;
                this.propertyChanged("Title");
            }
        }

        /// <summary>
        /// Url to website of book
        /// </summary>
        public string Url
        {
            get
            {
                return this.info.Url;
            }
            set
            {
                this.info.Url = value;
                this.propertyChanged("Url");
            }
        }

        /// <summary>
        /// Url to cover of book
        /// </summary>
        public string Cover
        {
            get
            {
                return this.info.Cover;
            }
            set
            {
                this.info.Cover = value;
                this.propertyChanged("Cvoer");
            }
        }

        /// <summary>
        /// Url to cover thomb of book
        /// </summary>
        public string CoverThumb
        {
            get
            {
                return this.info.CoverThumb;
            }
            set
            {
                this.info.CoverThumb = value;
                this.propertyChanged("CoverThumb");
            }
        }

        /// <summary>
        /// List of authors of book
        /// </summary>
        public List<Resource> Authors
        {
            get
            {
                return this.info.Authors;
            }
            set
            {
                this.info.Authors = value;
                this.propertyChanged("Authors");
            }
        }

        /// <summary>
        /// List of epochs of book
        /// </summary>
        public List<Resource> Epochs
        {
            get
            {
                return this.info.Epochs;
            }
            set
            {
                this.info.Epochs = value;
                this.propertyChanged("Epochs");
            }
        }

        /// <summary>
        /// List of genres of book
        /// </summary>
        public List<Resource> Genres
        {
            get
            {
                return this.info.Genres;
            }
            set
            {
                this.info.Genres = value;
                this.propertyChanged("Genres");
            }
        }

        /// <summary>
        /// List of kinds of book
        /// </summary>
        public List<Resource> Kinds
        {
            get
            {
                return this.info.Kinds;
            }
            set
            {
                this.info.Kinds = value;
                this.propertyChanged("Kinds");
            }
        }

        /// <summary>
        /// Parent of book
        /// </summary>
        public BookShortInfoViewModel Parent
        {
            get
            {
                if (this.info == null) return null;
                if (this.info.Parent == null) return null;
                return new BookShortInfoViewModel(this.info.Parent);
            }
            set
            {
                this.info.Parent = value.GetModel();
                this.propertyChanged("Parent");
            }
        }

        /// <summary>
        /// List of media of book
        /// </summary>
        public List<MediaResource> Media
        {
            get
            {
                return this.info.Media;
            }
            set
            {
                this.info.Media = value;
                this.propertyChanged("Media");
            }
        }

        /// <summary>
        /// List of children of book
        /// </summary>
        public List<BookShortInfoViewModel> Children
        {
            get
            {
                return this.info.Children.Select(x => new BookShortInfoViewModel(x)).ToList();
            }
            set
            {
                this.info.Children = value.Select(x => x.GetModel()).ToList();
                this.propertyChanged("Children");
            }
        }

        /// <summary>
        /// Method which returns stream with epub
        /// </summary>
        public Stream GetEpub()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Method which returns stream with pdf
        /// </summary>
        public Stream GetPdf()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Method which returns stream with mobi
        /// </summary>
        public Stream GetMobi()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method which returns string with txt
        /// </summary>
        public string GetTxt()
        {
            return ConnectionWL.GetBook(this.info);
        }
        /// <summary>
        /// Method which returns string with html
        /// </summary>
        public string GetHtml()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method which returns xml document with rdf
        /// </summary>
        public XmlDocument GetRdf()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Method which returns xml document with fictionbook
        /// </summary>
        public XmlDocument GetFictionBook()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Simply PropertyChanged method
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        private void propertyChanged(string name)
        {
            if (this.PropertyChanged != null) this.PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        /// <summary>
        /// Method which saves serialized to xml model
        /// </summary>
        /// <param name="filename">path tto file</param>
        public void SaveXml(string filename)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(BookInfo));
            FileStream fs = new FileStream(filename, FileMode.Create);
            serializer.Serialize(fs, this.info);
            fs.Close();
        }
        /// <summary>
        /// Method which load and deserialize model from xml
        /// </summary>
        /// <param name="filename">path tto file</param>
        public void LoadXml(string filename)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(BookInfo));
            FileStream fs = new FileStream(filename, FileMode.Open);
            this.info = serializer.Deserialize(fs) as BookInfo;
            fs.Close();
        }

        /// <summary>
        /// Method which returns model
        /// </summary>
        public BookInfo GetModel()
        {
            return this.info;
        }
    }
}
