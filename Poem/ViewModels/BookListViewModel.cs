﻿using Connection.Responses.BookList;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Utilities;
using Utilities.Interfaces;

namespace Poem.ViewModels
{
    /// <summary>
    /// ViewModel class for list of books
    /// </summary>
    public class BookListViewModel : INotifyPropertyChanged, ICanSerializeModel, IModelContainer<BooksList>
    {
        /// <summary>
        /// Observable collection for books' short info
        /// </summary>
        public ObservableCollection<BookShortInfoViewModel> Books { get; set; }
        private BooksList books;

        /// <summary>
        /// Constructor which can initialize Authors, Genres, Kinds and Epochs collections
        /// </summary>
        /// <param name="books">model</param>
        /// <param name="initialize">indicate when collections must be initialized</param>
        public BookListViewModel(BooksList books, bool initialize = false)
        {
            this.books = books;
            this.Books = new ObservableCollection<BookShortInfoViewModel>(books.Resources.Select(x => new BookShortInfoViewModel(x)));
            if (initialize)
            {
                Authors = new HashSet<string>(new List<string>(){ Any }.Concat(books.Resources.Select(x => x.Author).Sort()));
                Genres = new HashSet<string>(new List<string>() { Any }.Concat(books.Resources.Select(x => x.Genre).Sort()));
                Kinds = new HashSet<string>(new List<string>() { Any }.Concat(books.Resources.Select(x => x.Kind).Sort()));
                Epochs = new HashSet<string>(new List<string>() { Any }.Concat(books.Resources.Select(x => x.Epoch).Sort()));
            }
        }

        private string author = Any;
        private string genre = Any;
        private string kind = Any;
        private string epoch = Any;
        private string title = "";

        /// <summary>
        /// Selected author
        /// </summary>
        public string Author
        {
            get
            {
                return author;
            }
            set
            {
                if (this.author != value)
                {
                    this.author = value;
                    this.propertyChanged("Author");
                    this.Filter(this.filterFunction);
                }
            }
        }
        /// <summary>
        /// Selected genre
        /// </summary>
        public string Genre
        {
            get
            {
                return genre;
            }
            set
            {
                if (this.genre != value)
                {
                    this.genre = value;
                    this.propertyChanged("Genre");
                    this.Filter(this.filterFunction);
                }
            }
        }
        /// <summary>
        /// Selected kind
        /// </summary>
        public string Kind
        {
            get
            {
                return kind;
            }
            set
            {
                if (this.kind != value)
                {
                    this.kind = value;
                    this.propertyChanged("Kind");
                    this.Filter(this.filterFunction);
                }
            }
        }
        /// <summary>
        /// Selected epoch
        /// </summary>
        public string Epoch
        {
            get
            {
                return epoch;
            }
            set
            {
                if (this.epoch != value)
                {
                    this.epoch = value;
                    this.propertyChanged("Epoch");
                    this.Filter(this.filterFunction);
                }
            }
        }
        /// <summary>
        /// Filter for title
        /// </summary>
        public string Title
        {
            get
            {
                return this.title;
            }
            set
            {
                if (this.title != value)
                {
                    this.title = value;
                    this.propertyChanged("Title");
                    this.Filter(this.filterFunction);
                }
            }
        }

        private bool filterFunction(object o)
        {
            BookShortInfoViewModel model = o as BookShortInfoViewModel;
            if (!model.Title.ToLower().Contains(this.Title.ToLower())) return false;
            if (this.Author != Any && this.Author != model.Author) return false;
            if (this.Genre != Any && this.Genre != model.Genre) return false;
            if (this.Kind != Any && this.Kind != model.Kind) return false;
            if (this.Epoch != Any && this.Epoch != model.Epoch) return false;
            return true;
        }
        /// <summary>
        /// Execute when new filter must be apply
        /// </summary>
        public event FilterEventHandler Filter;

        /// <summary>
        /// Simply PropertyChanged method
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        private void propertyChanged(string name)
        {
            if (this.PropertyChanged != null) this.PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        /// <summary>
        /// Set of options for authors
        /// </summary>
        public static HashSet<string> Authors { get; set; }
        /// <summary>
        /// Set of options for genres
        /// </summary>
        public static HashSet<string> Genres { get; set; }
        /// <summary>
        /// Set of options for kinds
        /// </summary>
        public static HashSet<string> Kinds { get; set; }
        /// <summary>
        /// Set of options for epochs
        /// </summary>
        public static HashSet<string> Epochs { get; set; }

        /// <summary>
        /// Text of option which matches each element
        /// </summary>
        public const string Any = "<-- Any -->";

        /// <summary>
        /// Method which saves serialized to xml model
        /// </summary>
        /// <param name="filename">path tto file</param>
        public void SaveXml(string filename)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(BooksList));
            FileStream fs = new FileStream(filename, FileMode.Create);
            serializer.Serialize(fs, this.books);
            fs.Close();
        }
        /// <summary>
        /// Method which load and deserialize model from xml
        /// </summary>
        /// <param name="filename">path tto file</param>
        public void LoadXml(string filename)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(BooksList));
            FileStream fs = new FileStream(filename, FileMode.Open);
            this.books = serializer.Deserialize(fs) as BooksList;
            fs.Close();
        }

        /// <summary>
        /// Method which returns model
        /// </summary>
        public BooksList GetModel()
        {
            return this.books;
        }
    }

    /// <summary>
    /// delegate witch matches function which get predicate as argument
    /// </summary>
    /// <param name="predicate">function which check given BookInfoViewModel is matching to pattern</param>
    public delegate void FilterEventHandler(Predicate<object> predicate);
}
