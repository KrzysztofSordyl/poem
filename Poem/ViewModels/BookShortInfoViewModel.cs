﻿using Connection;
using Connection.Responses.BookInfo;
using Connection.Responses.BookList;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Serialization;
using Utilities.Interfaces;

namespace Poem.ViewModels
{
    /// <summary>
    /// ViewModel class for book short info
    /// </summary>
    public class BookShortInfoViewModel : INotifyPropertyChanged, ICanSerializeModel, IModelContainer<BookShortInfo>
    {
        private BookShortInfo info;

        /// <summary>
        /// Simply ViewModel constructor
        /// </summary>
        /// <param name="info">model</param>
        public BookShortInfoViewModel(BookShortInfo info)
        {
            this.info = info;
        }
        
        /// <summary>
        /// Kind of book
        /// </summary>
        public string Kind
        {
            get
            {
                return this.info.Kind;
            }
            set
            {
                this.info.Kind = value;
                this.propertyChanged("Kind");
            }
        }

        /// <summary>
        /// Author of book
        /// </summary>
        public string Author
        {
            get
            {
                return this.info.Author;
            }
            set
            {
                this.info.Author = value;
                this.propertyChanged("Author");
            }
        }

        /// <summary>
        /// Url to website of book
        /// </summary>
        public string Url
        {
            get
            {
                return this.info.Url;
            }
            set
            {
                this.info.Url = value;
                this.propertyChanged("Url");
            }
        }

        /// <summary>
        /// Title of book
        /// </summary>
        public string Title
        {
            get
            {
                return this.info.Title;
            }
            set
            {
                this.info.Title = value;
                this.propertyChanged("Title");
            }
        }

        /// <summary>
        /// Url to cover of book
        /// </summary>
        public string Cover
        {
            get
            {
                return this.info.Cover;
            }
            set
            {
                this.info.Cover = value;
                this.propertyChanged("Cover");
            }
        }

        /// <summary>
        /// Epoch of book
        /// </summary>
        public string Epoch
        {
            get
            {
                return this.info.Epoch;
            }
            set
            {
                this.info.Epoch = value;
                this.propertyChanged("Epoch");
            }
        }

        /// <summary>
        /// Url to API of book
        /// </summary>
        public string Href
        {
            get
            {
                return this.info.Href;
            }
            set
            {
                this.info.Href = value;
                this.propertyChanged("Href");
            }
        }

        /// <summary>
        /// Genre of book
        /// </summary>
        public string Genre
        {
            get
            {
                return this.info.Genre;
            }
            set
            {
                this.info.Genre = value;
                this.propertyChanged("Genre");
            }
        }

        /// <summary>
        /// Url to cover thumb of book
        /// </summary>
        public string CoverThumb
        {
            get
            {
                return this.info.CoverThumb;
            }
            set
            {
                this.info.CoverThumb = value;
                this.propertyChanged("CoverThumb");
            }
        }

        /// <summary>
        /// Simply PropertyChanged method
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        private void propertyChanged(string name)
        {
            if (this.PropertyChanged != null) this.PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        /// <summary>
        /// Method which gets more info about book
        /// </summary>
        /// <returns>viewModel for full info about book</returns>
        public BookInfoViewModel GetMoreInfo()
        {
            try
            {
                return new BookInfoViewModel(ConnectionWL.GetBookInfo(this.info));
            }
            catch
            {
                MessageBox.Show("Dowloading error", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }

        /// <summary>
        /// Method which saves serialized to xml model
        /// </summary>
        /// <param name="filename">path tto file</param>
        public void SaveXml(string filename)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(BookShortInfo));
            FileStream fs = new FileStream(filename, FileMode.Create);
            serializer.Serialize(fs, this.info);
            fs.Close();
        }
        /// <summary>
        /// Method which load and deserialize model from xml
        /// </summary>
        /// <param name="filename">path tto file</param>
        public void LoadXml(string filename)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(BookShortInfo));
            FileStream fs = new FileStream(filename, FileMode.Open);
            this.info = serializer.Deserialize(fs) as BookShortInfo;
            fs.Close();
        }

        /// <summary>
        /// Method which returns model
        /// </summary>
        public BookShortInfo GetModel()
        {
            return this.info;
        }
    }
}