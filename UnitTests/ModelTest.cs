﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Connection.Responses.BookInfo;
using System.Collections.Generic;
using Connection.Responses.BookList;
using Poem.ViewModels;
using System.IO;

namespace UnitTests
{
    [TestClass]
    public class ModelTest
    {
        [TestMethod]
        public void BookInfoTest()
        {
            #region createBookInfo
            BookInfo bi = new BookInfo()
            {
                Authors = new List<Resource>()
                {
                    new Resource()
                    {
                        Href = "qwerty",
                        Name = "asdfgh",
                        Url = "zxcvbn"
                    },
                    new Resource()
                    {
                        Href = "qaz",
                        Name = "wsx",
                        Url = "edc",
                    }
                },
                Children = new List<BookShortInfo>()
                {
                    new BookShortInfo()
                    {
                        Author = "rfv",
                        Cover = "tgb",
                        CoverThumb = "yhn",
                        Epoch = "ujm",
                        Genre = "ik,",
                        Href = "ol.",
                        Kind = "p;/",
                        Title = "123",
                        Url = "456"
                    },
                    new BookShortInfo()
                    {
                        Author = "789",
                        Cover = "0-=",
                        CoverThumb = "1qaz",
                        Epoch = "2wsx",
                        Genre = "3edc",
                        Href = "4rfv",
                        Kind = "5tgb",
                        Title = "6yhn",
                        Url = "7ujm"
                    }
                },
                Cover = "8ik,",
                CoverThumb = "9ol.",
                Epochs = new List<Resource>()
                {
                    new Resource()
                    {
                        Href = "0p;/",
                        Name = "111",
                        Url = "222"
                    },
                    new Resource()
                    {
                        Href = "333",
                        Name = "444",
                        Url = "555"
                    }
                },
                Epub = "666",
                Fb2 = "777",
                Genres = new List<Resource>()
                {
                    new Resource()
                    {
                        Href = "888",
                        Name = "999",
                        Url = "000"
                    },
                    new Resource()
                    {
                        Href = "---",
                        Name = "===",
                        Url = "qqq"
                    }
                },
                Html = "www",
                Kinds = new List<Resource>()
                {
                    new Resource()
                    {
                        Href = "eee",
                        Name = "rrr",
                        Url = "ttt"
                    },
                    new Resource()
                    {
                        Href = "yyy",
                        Name = "uuu",
                        Url = "iii"
                    }
                },
                Media = new List<MediaResource>()
                {
                    new MediaResource()
                    {
                        Artist = "ooo",
                        Director = "ppp",
                        Name = "aaa",
                        Type = "sss",
                        Url = "ddd"
                    },
                    new MediaResource()
                    {
                        Artist = "fff",
                        Director = "ggg",
                        Name = "hhh",
                        Type = "jjj",
                        Url = "kkk"
                    }
                },
                Mobi = "lll",
                Parent = new BookShortInfo()
                {
                    Author = ";;;",
                    Cover = "zzz",
                    CoverThumb = "xxx",
                    Epoch = "ccc",
                    Genre = "vvv",
                    Href = "bbb",
                    Kind = "nnn",
                    Title = "mmm",
                    Url = ",,,"
                },
                Pdf = "...",
                Title = "///",
                Txt = "[]'",
                Url = "[[[",
                Xml = "]]]"
            };
            #endregion

            BookInfoViewModel bivm = new BookInfoViewModel(bi);
            string path = "BookInfoTest.xml";
            try
            {
                bivm.SaveXml(path);
                bivm.LoadXml(path);
            }
            catch (Exception ex)
            {
                File.Delete(path);
                throw ex;
            }
            File.Delete(path);
            Assert.AreEqual(bi, bivm.GetModel());
        }

        [TestMethod]
        public void BookShortInfoTest()
        {
            #region createBookShortInfo
            BookShortInfo bsi = new BookShortInfo()
            {
                 Author = "qwe",
                 Cover = "rty",
                 CoverThumb = "uiop",
                 Epoch = "asd",
                 Genre = "fgh",
                 Href = "jkl",
                 Kind = "zxc",
                 Title = "vbnm",
                 Url = ",./"
            };
            #endregion

            BookShortInfoViewModel bsivm = new BookShortInfoViewModel(bsi);
            string path = "BookShortInfoTest.xml";
            try
            {
                bsivm.SaveXml(path);
                bsivm.LoadXml(path);
            }
            catch(Exception ex)
            {
                File.Delete(path);
                throw ex;
            }
            File.Delete(path);
            Assert.AreEqual(bsi, bsivm.GetModel());
        }
    }
}
