﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Connection.Responses.BookList;
using Connection;
using System.Windows.Controls;
using Connection.Responses.BookInfo;
using ProgressWindow;
using System.Collections.Generic;
using System.Threading;
using System.Windows;

namespace UnitTests
{
    [TestClass]
    public class ConnectionWLTest
    {
        [TestMethod]
        public void PoemsTest()
        {
            Progress p = new Progress();
            Thread thread = new Thread(new ThreadStart(() =>
            {
                MainWindow pw = new MainWindow(p);
                pw.ShowDialog();
            }));
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            WebBrowser wb = new WebBrowser();
            List<BookShortInfo> list = ConnectionWL.GetPoemsList().Resources;
            p.Max = list.Count;
            foreach (BookShortInfo bsi in list)
            {
                try
                {
                    BookInfo info = ConnectionWL.GetBookInfo(bsi);
                    Assert.IsNotNull(info);
                    if (string.IsNullOrEmpty(info.Html) && string.IsNullOrEmpty(info.Mobi))
                        Assert.Fail("Deserialized not correctly", p.Value);
                }
                catch(Exception ex)
                {
                    Assert.Fail(ex.Message, p.Value);
                }
                p.Value++;
            }
        }
    }
}
